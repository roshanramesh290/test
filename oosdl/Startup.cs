﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(oosdl.Startup))]
namespace oosdl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
